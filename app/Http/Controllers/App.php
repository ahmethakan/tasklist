<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\DB;
use App\Models\Task;

use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;

class App extends Controller
{
    public function index() {
        
        $this->getResponse('http://www.mocky.io/v2/5d47f24c330000623fa3ebfa', 1, 'id', 'zorluk', 'sure');

        $this->getResponse('http://www.mocky.io/v2/5d47f235330000623fa3ebf7', 2, "Business Task ", 'level', 'estimated_duration');
        
        $taskList = $this->distributionOfTasks();

        $time = $taskList[0][0];

        unset($taskList[0]);

        return view('plan', compact('taskList', 'time'));
    }

    public function distributionOfTasks() {

        $tasks = Task::all();

        $taskList = [];

        // dev1 = 1x, dev2 = 2x, dev3 = 3x, dev4 = 4x, dev5 = 5x
        $devsToplam = 15;

        $toplam = 0;

        // toplam gereken zorluk*zamanı hesapla ve işçilere paylaştır.
        foreach($tasks as $table)
        {
            $toplam = $toplam + $table->duration * $table->level;
        }
        $totalTime = [];
        $totalTime[0] = ($toplam/$devsToplam);

        array_push($taskList, $totalTime);

        for($i=1; $i<=5 ; $i++) {

            $devTasks = [];

            // developerin 1 haftada toplam kaç zorluk*zaman yapabileceğini hesapla
            $a = ($toplam/$devsToplam)*$i;

            $araToplam=0;

            // zorluk ve zamanı çarp bir developerin toplam zamanından küçükse
            // developera ekle ve daha sonra listeden sil

            // hangi görevin kime gittiğinin bir önemi yok zaman ve zorluk tutuyorsa
            // yeterli olacaktır

            foreach($tasks as $key => $table)
            {
                $gorevSuresi=0;
                $gorevSuresi = $gorevSuresi + $table->duration * $table->level;
                if(ceil($a) > $araToplam) {
                    array_push($devTasks, $table->taskname);
                    array_push($devTasks, $gorevSuresi/$i);
                    unset($tasks[$key]);
                    $araToplam = $araToplam + $gorevSuresi;
                }
            }
            
            // print_r($devTasks);
            array_push($taskList, $devTasks);
        }
        return $taskList;
        
    }

    // First item task ID, second item Level, third item Estimated Duration 
    public function getResponse($link, $type, $firstItem, $secondItem, $thirdItem) {

        $client = new Client();
        // request
        $api_response = $client->get($link);
        $response = json_decode($api_response->getBody(), true);

        $this->parseJson($response, $type, $firstItem, $secondItem, $thirdItem);
    }

    // First item task ID, second item Level, third item Estimated Duration 
    public function parseJson($response, $type, $firstItem, $secondItem, $thirdItem) {
        
        if($type==1) {
            for ($i = 0; $i < count($response); $i++) {
                $this->addDB($response[$i][$firstItem], $response[$i][$secondItem], $response[$i][$thirdItem]);
            }
        } else {
            for ($i = 0; $i < count($response); $i++) {
                $data = $firstItem.strval($i);
                $this->addDB($data, $response[$i][$data][$secondItem], $response[$i][$data][$thirdItem]);
            }
        }
    }

    public function addDB($taskName, $level, $duration) {

        // new model
        $tasks = new Task;

        $tasks->taskname = $taskName;
        $tasks->level = $level;
        $tasks->duration = $duration;

        // DB save
        $tasks->save();
    }
}
