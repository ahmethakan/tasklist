<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>İş Dağılımı</title>
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="alert alert-success" role="alert">
            <center>
                Yaklaşık olarak {{number_format($time/45, 2)}} hafta sürer.
            </center>
        </div>
        <div class="row">
            <div class="col-1">
            </div>
            @foreach ($taskList as $key =>$item)
                <div class="col-2">
                    <ul class="list-group">
                        <li class="list-group-item active" aria-current="true">Developer {{$key}}</li>  
                        @foreach ($item as $key2 => $a)           
                                         
                            @if ($key2%2 == 1)
                                <li class="list-group-item">{{number_format($a, 1)}} saat sürer.</li>
                            @else
                                <li class="list-group-item">{{$a}}</li>
                            @endif
                        @endforeach
                    </ul>
                </div>  
            @endforeach
        </div>
    </div>
    <script src="/js/app.js"></script>
</body>
</html>